public class Student{
	
	private int rScore;
	public int getRScore(){
		return this.rScore;
	}
	public void setRScore(int newrScore){
		this.rScore = newrScore;
	}
	
	private String spec;
	public String getSpec(){
		return this.spec;
	}
	public void setSpec(String newspec){
		this.spec = newspec;
	}
	
	private int grade; //education level grade 1 to 12
	public int getGrade(){
		return this.grade;
	}
	public void setGrade(int newgrade){
		this.grade = newgrade;
	}
	
	private int amountLearnt;
	public int getamountLearnt(){
		return this.amountLearnt;
	}
	public void setAmountLearnt(int newamountLearnt){
		this.amountLearnt = newamountLearnt;
	}
	
	public void graduatesIn(){
		grade = 13-grade;
		System.out.println("I graduate in "+grade+" years");
	}
	
	public void rstate(){
		if(rScore < 25){
			System.out.println("Work harder");
		}
		else if(rScore >= 25 && rScore < 30){
			System.out.println("You're almost there!");
		}
		else if(rScore >= 30 && rScore < 35){
			System.out.println("Wow good job!");
		}
		else{
			System.out.println("You're an academic weapon!");
		}
	}
		
	public void learn(int amountStudied){
			if(amountStudied > 0){
				amountLearnt = amountStudied;
			}
			else{
				amountLearnt = 0;
			}
	}
	
	public Student(String spec, int grade){
		this.spec = spec;
		this.grade = grade;
		this.rScore = 25;
		this.amountLearnt = 0;
	}
}